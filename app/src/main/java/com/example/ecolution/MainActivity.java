package com.example.ecolution;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.fonts.Font;
import android.graphics.fonts.FontFamily;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity {
    boolean backpackerrank = true;
    boolean travelerrank = false;
    boolean explorerrank = false;
    boolean masterrank = false;



    int checkerstatus = 0;

    int days = 1;

    final int Complete = 2000;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);







        connectXML();
        InitializeXP();
        assign();

        ArrayList<String> task = new ArrayList<>();
        task.add("Pick a vegeterian meal for dinner");
        task.add("Compost all of your food waste today");
        task.add("Use only reuseable bags today");
        task.add("Bring your own utensils and containers with you");
        task.add("Walk, bike or take public transit to work today");
        task.add("Take a cold shower today");
        task.add("Bring a reusable water bottle with you today");



        Random random = new Random();


//        int r = random.nextInt(task.size());
//        String rs = task.get(r);
//        task.remove(r);
        textview1.setText(task.get(0));

//        int r1 = random.nextInt(task.size());
//        String rs1 = task.get(r1);
//        task.remove(r1);
        textview2.setText(task.get(5));


        rankview.setText("Backpacker");

        fadein = AnimationUtils.loadAnimation(this, R.anim.fadein);
        topanim = AnimationUtils.loadAnimation(this, R.anim.top_animation);



    }


    void assign() {


        INFO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new MaterialAlertDialogBuilder(MainActivity.this, R.style.MyRounded_MaterialComponents_MaterialAlertDialog);
                AlertDialog ad = b.create();
                ad.setTitle("INFO");
                ad.setMessage("Reducing food waste to lower greenhouse gas emissions or reducing plastic use can substantially reduce your footprint!");
                ad.setCancelable(true);
                ad.show();
            }
        });

        TIPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new MaterialAlertDialogBuilder(MainActivity.this, R.style.MyRounded_MaterialComponents_MaterialAlertDialog);
                AlertDialog ad = b.create();
                ad.setCancelable(true);
                ad.setTitle("TIP");
                ad.setMessage("To combat plastic use, bring your own stuff from home. In some cases, this will also save you money");
                ad.show();

            }
        });

        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
                AlertDialog ad = b.create();
                img.setAnimation(fadein);
                ad.setView(img);
                ad.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                ad.setCancelable(true);

                checkerstatus += 1;

                if (checkerstatus == 2){
                    days += 1;
                    streak.setText(days + " days");
                    checkerstatus = 0;
                }

                if(img.getParent() != null) {
                    ((ViewGroup) img.getParent()).removeView(img);

                }
                ad.show();
                int max = seekBar.getMax();
                float current = seekBar.getProgress();
                seekBar.setProgress((int) (current + 40));
                XP.setText(seekBar.getProgress() + "/" + max);
                if (seekBar.getProgress() >= 200 && backpackerrank == true){
                    backpackerrank = false;
                    travelerrank = true;
                    rank.setImageResource(R.drawable.travelerrank);
                    seekBar.setProgress(0);
                    rankview.setText("Traveler");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);

                }

                if (seekBar.getProgress() >= 200 && travelerrank == true){
                    travelerrank = false;
                    explorerrank = true;
                    rank.setImageResource(R.drawable.explorerrank);
                    seekBar.setProgress(0);
                    rankview.setText("Explorer");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);

                }

                if (seekBar.getProgress() >= 200 && explorerrank == true){
                    explorerrank = false;
                    masterrank = true;
                    rank.setImageResource(R.drawable.masterrank);
                    seekBar.setProgress(0);
                    rankview.setText("Master");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);

                }

                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        ad.dismiss();
                    }
                });

            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AlertDialog.Builder b = new AlertDialog.Builder(MainActivity.this);
                AlertDialog ad = b.create();
                if(img.getParent() != null) {
                    ((ViewGroup) img.getParent()).removeView(img);

                }
                img.setAnimation(fadein);
                ad.setView(img);
                ad.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                ad.setCancelable(true);
                ad.show();

                checkerstatus += 1;

                if (checkerstatus == 2){
                    days += 1;
                    streak.setText(days + " days");
                    checkerstatus = 0;
                }

                int max = seekBar.getMax();
                float current = seekBar.getProgress();
                seekBar.setProgress((int) (current + 40));
                XP.setText(seekBar.getProgress() + "/" + max);
                if (seekBar.getProgress() >= 200 && backpackerrank == true){
                    backpackerrank = false;
                    travelerrank = true;
                    rank.setImageResource(R.drawable.travelerrank);
                    seekBar.setProgress(0);
                    rankview.setText("Traveler");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);
                    seekBar.getProgressDrawable();

                }

                if (seekBar.getProgress() >= 200 && travelerrank == true){
                    travelerrank = false;
                    explorerrank = true;
                    rank.setImageResource(R.drawable.explorerrank);
                    seekBar.setProgress(0);
                    rankview.setText("Explorer");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);

                }

                if (seekBar.getProgress() >= 200 && explorerrank == true){
                    explorerrank = false;
                    masterrank = true;
                    rank.setImageResource(R.drawable.masterrank);
                    seekBar.setProgress(0);
                    rankview.setText("Master");
                    float update = seekBar.getProgress();
                    XP.setText(update + "/" + max);

                }

                ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        ad.dismiss();

                    }
                });
            }
        });
    }



    void connectXML(){
        TIPS = findViewById(R.id.tipbutton);
        INFO = findViewById(R.id.infobutton);
        img = new ImageView(this);
        img.setImageResource(R.drawable.complete1);
        XP = findViewById(R.id.textviewXP);
        seekBar = (SeekBar) findViewById(R.id.seekbar);
        checkBox1 = findViewById(R.id.checkbox1);
        checkBox2 = findViewById(R.id.checkbox2);
        streak = findViewById(R.id.daystreak);
        streak.setText(days + " days");
        textview1 = findViewById(R.id.textview1);
        textview2 = findViewById(R.id.textview2);
        rank = findViewById(R.id.rank);
        rankview = findViewById(R.id.ranktext);
    }

    void InitializeXP(){


        AlertDialog.Builder b = new MaterialAlertDialogBuilder(MainActivity.this, R.style.MyRounded_MaterialComponents_MaterialAlertDialog);
        AlertDialog dialog = b.create();
        dialog.setTitle("Welcome!");
        dialog.setCancelable(true);
        dialog.setMessage("Thanks for downloading the app!");
        dialog.setMessage("Here is 20 XP!");
        seekBar.setMin(0);
        seekBar.setProgress(80);
        seekBar.setMax(200);
        int s = (int) seekBar.getProgress();
        int smax = (int) seekBar.getMax();
        seekBar.setEnabled(false);
        XP.setText(s + "/" + smax);
        dialog.show();

    }









    //XML
    TextView textview1;
    TextView textview2;
    TextView streak;
    TextView XP;
    SeekBar seekBar;
    ImageView img;
    ImageView rank;
    CheckBox checkBox1;
    CheckBox checkBox2;
    AppCompatImageButton TIPS;
    AppCompatImageButton INFO;
    Animation fadein;
    Animation topanim;
    TextView rankview;
    //Private Properties


    //Public Properties

}